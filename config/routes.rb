Myapp::Application.routes.draw do
  resources :sales
  resources :days_goals
  resources :salesmen
  resources :sales_goals
  resources :stores
  devise_for :users, :controllers => {registrations: "registrations"}
  get "home/index"
  get "home/minor"
  root to: 'home#index'
end
