class CreateSalesmen < ActiveRecord::Migration[5.2]
  def change
    create_table :salesmen do |t|
      t.string :name
      t.references :sales_goals, foreign_key: true
      t.references :stores, foreign_key: true

      t.timestamps
    end
  end
end
