class CreateSales < ActiveRecord::Migration[5.2]
  def change
    create_table :sales do |t|
      t.references :day_goals, foreign_key: true
      t.float :value
      t.references :salesman, foreign_key: true

      t.timestamps
    end
  end
end
