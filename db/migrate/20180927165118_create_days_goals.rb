class CreateDaysGoals < ActiveRecord::Migration[5.2]
  def change
    create_table :days_goals do |t|
      t.references :sales_goals, foreign_key: true
      t.date :date_sales
      t.float :value

      t.timestamps
    end
  end
end
