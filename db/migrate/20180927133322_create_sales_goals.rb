class CreateSalesGoals < ActiveRecord::Migration[5.2]
  def change
    create_table :sales_goals do |t|
      t.date :dt_start
      t.date :dt_end
      t.date :ref_month
      t.float :value

      t.timestamps
    end
  end
end
