class DaysGoalsController < ApplicationController
  before_action :set_days_goal, only: [:show, :edit, :update, :destroy]

  # GET /days_goals
  # GET /days_goals.json
  def index
    @days_goals = DaysGoal.all
  end

  # GET /days_goals/1
  # GET /days_goals/1.json
  def show
  end

  # GET /days_goals/new
  def new
    @days_goal = DaysGoal.new
  end

  # GET /days_goals/1/edit
  def edit
  end

  # POST /days_goals
  # POST /days_goals.json
  def create
    @days_goal = DaysGoal.new(days_goal_params)

    respond_to do |format|
      if @days_goal.save
        format.html { redirect_to @days_goal, notice: 'Days goal was successfully created.' }
        format.json { render :show, status: :created, location: @days_goal }
      else
        format.html { render :new }
        format.json { render json: @days_goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /days_goals/1
  # PATCH/PUT /days_goals/1.json
  def update
    respond_to do |format|
      if @days_goal.update(days_goal_params)
        format.html { redirect_to @days_goal, notice: 'Days goal was successfully updated.' }
        format.json { render :show, status: :ok, location: @days_goal }
      else
        format.html { render :edit }
        format.json { render json: @days_goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /days_goals/1
  # DELETE /days_goals/1.json
  def destroy
    @days_goal.destroy
    respond_to do |format|
      format.html { redirect_to days_goals_url, notice: 'Days goal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_days_goal
      @days_goal = DaysGoal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def days_goal_params
      params.require(:days_goal).permit(:sales_goals_id, :date_sales, :value)
    end
end
