class SalesGoalsController < ApplicationController
  before_action :set_sales_goal, only: [:show, :edit, :update, :destroy]

  # GET /sales_goals
  # GET /sales_goals.json
  def index
    @sales_goals = SalesGoal.all
  end

  # GET /sales_goals/1
  # GET /sales_goals/1.json
  def show
  end

  # GET /sales_goals/new
  def new
    @sales_goal = SalesGoal.new
  end

  # GET /sales_goals/1/edit
  def edit
  end

  # POST /sales_goals
  # POST /sales_goals.json
  def create
    @sales_goal = SalesGoal.new(sales_goal_params)

    respond_to do |format|
      if @sales_goal.save
        format.html { redirect_to @sales_goal, notice: 'Sales goal was successfully created.' }
        format.json { render :show, status: :created, location: @sales_goal }
      else
        format.html { render :new }
        format.json { render json: @sales_goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sales_goals/1
  # PATCH/PUT /sales_goals/1.json
  def update
    respond_to do |format|
      if @sales_goal.update(sales_goal_params)
        format.html { redirect_to @sales_goal, notice: 'Sales goal was successfully updated.' }
        format.json { render :show, status: :ok, location: @sales_goal }
      else
        format.html { render :edit }
        format.json { render json: @sales_goal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sales_goals/1
  # DELETE /sales_goals/1.json
  def destroy
    @sales_goal.destroy
    respond_to do |format|
      format.html { redirect_to sales_goals_url, notice: 'Sales goal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sales_goal
      @sales_goal = SalesGoal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sales_goal_params
      params.require(:sales_goal).permit(:dt_start, :dt_end, :ref_month, :value)
    end
end
