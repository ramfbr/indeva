class Salesman < ApplicationRecord
  belongs_to :sales_goals
  belongs_to :stores
end
