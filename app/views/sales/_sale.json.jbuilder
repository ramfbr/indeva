json.extract! sale, :id, :day_goals_id, :value, :salesman_id, :created_at, :updated_at
json.url sale_url(sale, format: :json)
