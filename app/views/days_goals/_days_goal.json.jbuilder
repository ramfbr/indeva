json.extract! days_goal, :id, :sales_goals_id, :date_sales, :value, :created_at, :updated_at
json.url days_goal_url(days_goal, format: :json)
