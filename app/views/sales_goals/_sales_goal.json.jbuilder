json.extract! sales_goal, :id, :dt_start, :dt_end, :ref_month, :value, :created_at, :updated_at
json.url sales_goal_url(sales_goal, format: :json)
