json.extract! salesman, :id, :name, :sales_goals_id, :stores_id, :created_at, :updated_at
json.url salesman_url(salesman, format: :json)
