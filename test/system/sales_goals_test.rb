require "application_system_test_case"

class SalesGoalsTest < ApplicationSystemTestCase
  setup do
    @sales_goal = sales_goals(:one)
  end

  test "visiting the index" do
    visit sales_goals_url
    assert_selector "h1", text: "Sales Goals"
  end

  test "creating a Sales goal" do
    visit sales_goals_url
    click_on "New Sales Goal"

    fill_in "Dt End", with: @sales_goal.dt_end
    fill_in "Dt Start", with: @sales_goal.dt_start
    fill_in "Ref Month", with: @sales_goal.ref_month
    fill_in "Value", with: @sales_goal.value
    click_on "Create Sales goal"

    assert_text "Sales goal was successfully created"
    click_on "Back"
  end

  test "updating a Sales goal" do
    visit sales_goals_url
    click_on "Edit", match: :first

    fill_in "Dt End", with: @sales_goal.dt_end
    fill_in "Dt Start", with: @sales_goal.dt_start
    fill_in "Ref Month", with: @sales_goal.ref_month
    fill_in "Value", with: @sales_goal.value
    click_on "Update Sales goal"

    assert_text "Sales goal was successfully updated"
    click_on "Back"
  end

  test "destroying a Sales goal" do
    visit sales_goals_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Sales goal was successfully destroyed"
  end
end
