require "application_system_test_case"

class DaysGoalsTest < ApplicationSystemTestCase
  setup do
    @days_goal = days_goals(:one)
  end

  test "visiting the index" do
    visit days_goals_url
    assert_selector "h1", text: "Days Goals"
  end

  test "creating a Days goal" do
    visit days_goals_url
    click_on "New Days Goal"

    fill_in "Date Sales", with: @days_goal.date_sales
    fill_in "Sales Goals", with: @days_goal.sales_goals_id
    fill_in "Value", with: @days_goal.value
    click_on "Create Days goal"

    assert_text "Days goal was successfully created"
    click_on "Back"
  end

  test "updating a Days goal" do
    visit days_goals_url
    click_on "Edit", match: :first

    fill_in "Date Sales", with: @days_goal.date_sales
    fill_in "Sales Goals", with: @days_goal.sales_goals_id
    fill_in "Value", with: @days_goal.value
    click_on "Update Days goal"

    assert_text "Days goal was successfully updated"
    click_on "Back"
  end

  test "destroying a Days goal" do
    visit days_goals_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Days goal was successfully destroyed"
  end
end
