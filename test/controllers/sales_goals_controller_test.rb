require 'test_helper'

class SalesGoalsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sales_goal = sales_goals(:one)
  end

  test "should get index" do
    get sales_goals_url
    assert_response :success
  end

  test "should get new" do
    get new_sales_goal_url
    assert_response :success
  end

  test "should create sales_goal" do
    assert_difference('SalesGoal.count') do
      post sales_goals_url, params: { sales_goal: { dt_end: @sales_goal.dt_end, dt_start: @sales_goal.dt_start, ref_month: @sales_goal.ref_month, value: @sales_goal.value } }
    end

    assert_redirected_to sales_goal_url(SalesGoal.last)
  end

  test "should show sales_goal" do
    get sales_goal_url(@sales_goal)
    assert_response :success
  end

  test "should get edit" do
    get edit_sales_goal_url(@sales_goal)
    assert_response :success
  end

  test "should update sales_goal" do
    patch sales_goal_url(@sales_goal), params: { sales_goal: { dt_end: @sales_goal.dt_end, dt_start: @sales_goal.dt_start, ref_month: @sales_goal.ref_month, value: @sales_goal.value } }
    assert_redirected_to sales_goal_url(@sales_goal)
  end

  test "should destroy sales_goal" do
    assert_difference('SalesGoal.count', -1) do
      delete sales_goal_url(@sales_goal)
    end

    assert_redirected_to sales_goals_url
  end
end
