require 'test_helper'

class DaysGoalsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @days_goal = days_goals(:one)
  end

  test "should get index" do
    get days_goals_url
    assert_response :success
  end

  test "should get new" do
    get new_days_goal_url
    assert_response :success
  end

  test "should create days_goal" do
    assert_difference('DaysGoal.count') do
      post days_goals_url, params: { days_goal: { date_sales: @days_goal.date_sales, sales_goals_id: @days_goal.sales_goals_id, value: @days_goal.value } }
    end

    assert_redirected_to days_goal_url(DaysGoal.last)
  end

  test "should show days_goal" do
    get days_goal_url(@days_goal)
    assert_response :success
  end

  test "should get edit" do
    get edit_days_goal_url(@days_goal)
    assert_response :success
  end

  test "should update days_goal" do
    patch days_goal_url(@days_goal), params: { days_goal: { date_sales: @days_goal.date_sales, sales_goals_id: @days_goal.sales_goals_id, value: @days_goal.value } }
    assert_redirected_to days_goal_url(@days_goal)
  end

  test "should destroy days_goal" do
    assert_difference('DaysGoal.count', -1) do
      delete days_goal_url(@days_goal)
    end

    assert_redirected_to days_goals_url
  end
end
